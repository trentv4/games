package com.trentv4.pliable.ux;

import static com.trentv4.pliable.DisplayManager.height;
import static com.trentv4.pliable.DisplayManager.width;

import java.awt.Color;

import com.trentv4.pliable.DisplayManager;

public class InterfaceObject
{
    public double xScale = 1;
    public double yScale = 1;
    public int xSize = 0;
    public int ySize = 0;
    public Color color;
    public int x = 0;
    public int y = 0;

    public String texture = "";
    public String text = "";

    private boolean isUsingScale = false;

    public InterfaceObject setScale(double x, double y)
    {
        this.xScale = x;
        this.yScale = y;
        isUsingScale = true;
        return this;
    }

    public InterfaceObject setSize(int x, int y)
    {
        this.xSize = x;
        this.ySize = y;
        isUsingScale = false;
        return this;
    }

    public InterfaceObject setPosition(int x, int y)
    {
        this.x = x;
        this.y = y;
        return this;
    }

    public InterfaceObject setText(String text)
    {
        this.text = text;
        return this;
    }

    public InterfaceObject setTexture(String texture)
    {
        this.texture = texture;
        return this;
    }

    public InterfaceObject setColor(Color color)
    {
        this.color = color;
        return this;
    }

    public InterfaceObject setAlignment(AlignmentStyle style)
    {
        switch (style)
        {
        case LEFT:
            x = 0 + x;
        case RIGHT:
            x = width - xSize + x;
        case CENTER:
            x = ((width - xSize) / 2) + x;
        }
        return this;
    }

    public void render()
    {
        if (!text.equals(""))
        {
            if (isUsingScale)
            {
                DisplayManager.drawText(text, color, x, y, (int) (width * xScale));
            } else
            {
                DisplayManager.drawText(text, color, x, y, xSize);
            }
        } else if (!texture.equals(""))
        {
            if (isUsingScale)
            {
                DisplayManager.drawImage(texture, x, y, width * xScale, height * yScale);
            } else
            {
                DisplayManager.drawImage(texture, x, y, xSize, ySize);
            }
        } else
        {
            if (isUsingScale)
            {
                DisplayManager.drawRectangle(color, x, y, width * xScale, height * yScale);
            } else
            {
                DisplayManager.drawRectangle(color, x, y, xSize, ySize);
            }
        }
    }
}
