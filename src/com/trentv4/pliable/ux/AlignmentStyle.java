package com.trentv4.pliable.ux;

public enum AlignmentStyle
{
    LEFT, CENTER, RIGHT;
}
