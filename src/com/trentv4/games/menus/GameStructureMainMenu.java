package com.trentv4.games.menus;

import java.awt.Color;

import com.trentv4.pliable.DisplayManager;
import com.trentv4.pliable.GameStructure;
import com.trentv4.pliable.InputScenario;
import com.trentv4.pliable.ux.AlignmentStyle;
import com.trentv4.pliable.ux.InterfaceObject;

public class GameStructureMainMenu extends GameStructure
{
    public InterfaceObject[] userInterface;
    private int movement = 0;

    @Override
    public InputScenario getInputScenario()
    {
        if (scenario == null)
        {
            scenario = new InputScenarioMainMenu();
            scenario.structure = this;
            return scenario;
        } else
        {
            return scenario;
        }
    }

    @Override
    public void initialize()
    {
        userInterface = new InterfaceObject[] {
                new InterfaceObject().setSize(490, 80).setTexture("menu-490-80.png").setPosition(0, 10).setAlignment(AlignmentStyle.CENTER),
                new InterfaceObject().setSize(7, 0).setText("Pick your game!").setColor(new Color(255, 233, 66)).setPosition(-199, 32)
                        .setAlignment(AlignmentStyle.CENTER),

                new InterfaceObject().setSize(490, 490).setTexture("menu-490-490.png").setPosition(0, 100)
                        .setAlignment(AlignmentStyle.CENTER),

                new InterfaceObject().setSize(150, 150).setTexture("games/tetris/icon.png").setPosition(-160, 110)
                        .setAlignment(AlignmentStyle.CENTER),
        /*
         * new
         * InterfaceObject().setSize(150,150).setTexture("games/tetris/icon.png"
         * ).setPosition(0,110).setAlignment(AlignmentStyle.CENTER), new
         * InterfaceObject
         * ().setSize(150,150).setTexture("games/tetris/icon.png")
         * .setPosition(160,110).setAlignment(AlignmentStyle.CENTER),
         * 
         * new
         * InterfaceObject().setSize(150,150).setTexture("games/tetris/icon.png"
         * ).setPosition(-160,270).setAlignment(AlignmentStyle.CENTER), new
         * InterfaceObject
         * ().setSize(150,150).setTexture("games/tetris/icon.png")
         * .setPosition(0,270).setAlignment(AlignmentStyle.CENTER), new
         * InterfaceObject
         * ().setSize(150,150).setTexture("games/tetris/icon.png")
         * .setPosition(160,270).setAlignment(AlignmentStyle.CENTER),
         * 
         * new
         * InterfaceObject().setSize(150,150).setTexture("games/tetris/icon.png"
         * ).setPosition(-160,430).setAlignment(AlignmentStyle.CENTER), new
         * InterfaceObject
         * ().setSize(150,150).setTexture("games/tetris/icon.png")
         * .setPosition(0,430).setAlignment(AlignmentStyle.CENTER), new
         * InterfaceObject
         * ().setSize(150,150).setTexture("games/tetris/icon.png")
         * .setPosition(160,430).setAlignment(AlignmentStyle.CENTER)
         */
        };
    }

    @Override
    public void draw()
    {
        DisplayManager.drawImage("background.png", -800 + movement, 0, 1600, 600);

        if (userInterface != null)
        {
            for (int i = 0; i < userInterface.length; i++)
            {
                userInterface[i].render();
            }
        }
    }

    @Override
    public void tick()
    {
        if (movement < 800)
        {
            movement += 1;
        } else
        {
            movement = 0;
        }
    }
}
