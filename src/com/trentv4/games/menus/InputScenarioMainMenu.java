package com.trentv4.games.menus;

import static com.trentv4.pliable.InputMapper.getMouseButtons;
import static com.trentv4.pliable.InputMapper.getMousePos;

import com.trentv4.games.tetris.GameStructureTetris;
import com.trentv4.pliable.GameLoop;
import com.trentv4.pliable.InputScenario;
import com.trentv4.pliable.ux.InterfaceObject;

public class InputScenarioMainMenu extends InputScenario
{
    @Override
    public void tick()
    {
        int[] pos = getMousePos();
        GameStructureMainMenu struct = (GameStructureMainMenu) structure;
        if (isWithin(struct.userInterface[3], pos[0], pos[1]) && getMouseButtons()[0])
        {
            GameLoop.gameStack.pop();
            GameLoop.gameStack.push(new GameStructureTetris());
        }
    }

    private boolean isWithin(InterfaceObject o, int x, int y)
    {
        if (x > o.x)
        {
            if (x < o.x + o.xSize)
            {
                if (y > o.y)
                {
                    if (y < o.y + o.ySize)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
