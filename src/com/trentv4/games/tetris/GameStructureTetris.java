package com.trentv4.games.tetris;

import java.awt.Color;

import com.trentv4.pliable.DisplayManager;
import com.trentv4.pliable.GameStructure;
import com.trentv4.pliable.InputScenario;
import com.trentv4.pliable.ux.AlignmentStyle;
import com.trentv4.pliable.ux.InterfaceObject;

public class GameStructureTetris extends GameStructure
{
    public InterfaceObject[] userInterface;
    public TetrisBlock currentBlock;
    public boolean[] level = new boolean[16 * 20];

    private int mayTick = 0;

    @Override
    public InputScenario getInputScenario()
    {
        if (scenario == null)
        {
            scenario = new InputScenarioTetris();
            scenario.structure = this;
            return scenario;
        } else
        {
            return scenario;
        }
    }

    @Override
    public void initialize()
    {
        userInterface = new InterfaceObject[] { new InterfaceObject().setSize(7, 0).setText("Tetris <wip>")
                .setColor(new Color(255, 233, 66)).setPosition(-199, 32).setAlignment(AlignmentStyle.CENTER), };
    }

    @Override
    public void draw()
    {
        if (userInterface != null)
        {
            for (int i = 0; i < userInterface.length; i++)
            {
                userInterface[i].render();
            }
        }
        for (int y = 0; y < 20; y++)
        {
            for (int x = 0; x < 16; x++)
            {
                if (level[(y * 16 + x)])
                {
                    DisplayManager.drawRectangle(Color.red, (200) + x * 25, (75) + y * 25, 25, 25);
                } else
                {
                    DisplayManager.drawRectangle(Color.blue, (200) + x * 25, (75) + y * 25, 25, 25);
                }
            }
        }
        if (currentBlock != null)
        {
            int x2 = currentBlock.x * 25;
            int y2 = currentBlock.y * 25;
            for (int i = 0; i < currentBlock.model.length; i++)
            {
                for (int g = 0; g < currentBlock.model[i].length; g++)
                {
                    if (currentBlock.model[i][g])
                    {
                        DisplayManager.drawRectangle(Color.green, (200) + x2, (75) + y2, 25, 25);
                    }
                }
                y2 += 25;
            }
        }
    }

    @Override
    public void tick()
    {
        if (mayTick > 30)
        {
            mayTick = 0;
            if (currentBlock == null)
            {
                currentBlock = new TetrisBlock(TetrisBlock.line);
            } else
            {
                if (currentBlock.willCollide())
                {
                    int x2 = currentBlock.x;
                    int y2 = currentBlock.y;
                    for (int i = 0; i < currentBlock.model.length; i++)
                    {
                        for (int g = 0; g < currentBlock.model[i].length; g++)
                        {
                            if (currentBlock.model[i][g])
                            {
                                level[y2 * 16 + x2] = true;
                            }
                        }
                        y2++;
                    }
                    currentBlock = null;
                } else
                {
                    currentBlock.y++;
                }
            }
        } else
        {
            mayTick++;
        }
    }
}
