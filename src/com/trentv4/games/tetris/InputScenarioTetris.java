package com.trentv4.games.tetris;

import static com.trentv4.pliable.InputMapper.isPressed;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_A;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_D;

import com.trentv4.pliable.InputScenario;

public class InputScenarioTetris extends InputScenario
{
    @Override
    public void tick()
    {
        GameStructureTetris struct = (GameStructureTetris) structure;
        if (isPressed(GLFW_KEY_A))
        {
            if (struct.currentBlock != null)
            {
                struct.currentBlock.x--;
                if (struct.currentBlock.willCollide())
                {
                    struct.currentBlock.x++;
                }
            }
        }
        if (isPressed(GLFW_KEY_D))
        {
            if (struct.currentBlock != null)
            {
                struct.currentBlock.x++;
                if (struct.currentBlock.willCollide())
                {
                    struct.currentBlock.x--;
                }
            }
        }
    }
}
