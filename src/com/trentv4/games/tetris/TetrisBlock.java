package com.trentv4.games.tetris;

public class TetrisBlock
{
    public int x;
    public int y;
    public boolean model[][];
    public int length;
    public String texture;

    public static final TetrisBlock line = new TetrisBlock().setModel(new boolean[][] { { true }, { true }, { true }, { true }, { true } },
            1).setTexture("games/tetris/blue.png");

    public TetrisBlock(TetrisBlock block)
    {
        texture = block.texture;
        model = block.model;
        length = block.length;
        x = 0;
        y = 0;
    }

    private TetrisBlock()
    {
    }

    public TetrisBlock setModel(boolean[][] model, int length)
    {
        this.model = model;
        this.length = length;
        return this;
    }

    public TetrisBlock setTexture(String texture)
    {
        this.texture = texture;
        return this;
    }

    public boolean willCollide()
    {
        if (x < 0 | x > 16)
        {
            return true;
        }
        if (y > 12)
        {
            return true;
        }
        return false;
    }
}
