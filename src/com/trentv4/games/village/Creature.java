package com.trentv4.games.village;

import com.trentv4.pliable.DisplayManager;

public class Creature
{
    public String name;
    public int x, y;
    public int xSize, ySize;
    public String texture;

    public Creature(String name, String texture, int x, int y)
    {
        this.name = name;
        this.texture = texture;
        this.x = x * 50;
        this.y = y * 50;
        this.xSize = 50;
        this.ySize = 50;
    }

    public Creature(String name, String texture, int x, int y, int xSize, int ySize)
    {
        this.name = name;
        this.texture = texture;
        this.x = x;
        this.y = y;
        this.xSize = xSize * 50;
        this.ySize = ySize * 50;
    }

    public void move(int x, int y)
    {
        this.x += x;
        this.y += y;
    }

    public void render()
    {
        DisplayManager.drawImage(texture, x + GameStructureVillage.cameraX, y + GameStructureVillage.cameraY, xSize, ySize);
    }
}
