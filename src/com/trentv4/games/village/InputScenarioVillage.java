package com.trentv4.games.village;

import static com.trentv4.pliable.InputMapper.*;
import static org.lwjgl.glfw.GLFW.*;

import com.trentv4.pliable.InputScenario;

public class InputScenarioVillage extends InputScenario
{
    @Override
    public void tick()
    {
        GameStructureVillage struct = (GameStructureVillage) structure;
        if (isDown(GLFW_KEY_W))
        {
            struct.player.move(0, -2);
        }
        if (isDown(GLFW_KEY_A))
        {
            struct.player.move(-2, 0);
        }
        if (isDown(GLFW_KEY_S))
        {
            struct.player.move(0, 2);
        }
        if (isDown(GLFW_KEY_D))
        {
            struct.player.move(2, 0);
        }
        if (isPressed(GLFW_KEY_SPACE))
        {

        }
    }
}
