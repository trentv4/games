package com.trentv4.games.village;

import com.trentv4.pliable.DisplayManager;
import com.trentv4.pliable.GameStructure;
import com.trentv4.pliable.InputScenario;
import com.trentv4.pliable.ux.InterfaceObject;

public class GameStructureVillage extends GameStructure
{
    public InterfaceObject[] userInterface;
    public Creature[] creatures;
    public WorldObject[] world;
    public static int cameraX = 0, cameraY = 0;
    public Creature player;

    @Override
    public InputScenario getInputScenario()
    {
        if (scenario == null)
        {
            scenario = new InputScenarioVillage();
            scenario.structure = this;
            return scenario;
        } else
        {
            return scenario;
        }
    }

    @Override
    public void initialize()
    {
        userInterface = new InterfaceObject[] {

        };
        world = new WorldObject[] {
                // Background
                new WorldObject("games/village/wobj/grass.png", 0, 0, 25, 25),
                new WorldObject("games/village/wob/cottage1.png", 5, 5, 2, 2) };
        creatures = new Creature[] { player = new Creature("player", "games/tetris/icon.png", 1, 1),
                new Creature("basic villager", "games/village/villager1.png", 2, 2) };
    }

    @Override
    public void draw()
    {
        if (player != null)
        {
            cameraX = -player.x - (player.xSize / 2) + (DisplayManager.width / 2);
            cameraY = -player.y - (player.ySize / 2) + (DisplayManager.height / 2) + 50;
            // I call this my magic sauce
        }
        if (world != null)
        {
            for (int i = 0; i < world.length; i++)
            {
                world[i].render();
            }
        }
        if (creatures != null)
        {
            for (int i = 0; i < creatures.length; i++)
            {
                creatures[i].render();
            }
        }
        if (userInterface != null)
        {
            for (int i = 0; i < userInterface.length; i++)
            {
                userInterface[i].render();
            }
        }
    }

    @Override
    public void tick()
    {

    }
}
