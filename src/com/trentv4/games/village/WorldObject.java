package com.trentv4.games.village;

import com.trentv4.pliable.DisplayManager;

public class WorldObject
{
    public int x, y;
    public int xSize, ySize;
    public String texture;

    public WorldObject(String texture, int x, int y)
    {
        this.texture = texture;
        this.x = x * 50;
        this.y = y * 50;
        this.xSize = 1;
        this.ySize = 1;
    }

    public WorldObject(String texture, int x, int y, int xSize, int ySize)
    {
        this.texture = texture;
        this.x = x;
        this.y = y;
        this.xSize = xSize;
        this.ySize = ySize;
    }

    public void render()
    {
        DisplayManager.drawImage(texture, x + GameStructureVillage.cameraX, y + GameStructureVillage.cameraY, xSize * 50, ySize * 50);
    }
}
